# Доработайте классы Point и Line из занятия. Обеспечьте передачу в атрибуты x и y класса Point только чисел, а в
# атрибуты
# start_point и end_point класса Line только обьектов класса Point
# Реализуйте в классе Point механизм сложения таким образом, чтобы при сложении двух точек получался обьект класса Line

# class Point:
#     x = 0
#     y = 0
#
#     def __init__(self, new_x, new_y):
#         if not isinstance(new_x, (int, float)) or not isinstance(new_y, (int, float)):
#             raise TypeError
#
#         self.x = new_x
#         self.y = new_y

class OnlyIntDescriptor:

    def __set__(self, instance, value):
        print(f'__set__ {instance} {value}')
        if isinstance(value, (int, float)):
            setattr(self, '_value', value)
        else:
            raise TypeError

    def __get__(self, instance, owner):
        print(f'__get__ {instance}')

        return getattr(self, '_value')


class Point:
    x = OnlyIntDescriptor()
    y = OnlyIntDescriptor()

    def __init__(self, new_x, new_y):
        self.x = new_x
        self.y = new_y

    # def __add__(self, other):
    #     return Point(self.x + other.x, self.y + other.y)


point1 = Point(1, 2)


class Line:

    start_point = point1.x
    end_point = point1.y

    def __add__(self, other):
        return Point(self.start_point + other.start_point, self.end_point + other.end_point)


print(point1.x)
print(point1.y)
print(point1.x + point1.y)



