# Пишем игру )
# Программа выбирает из диапазона чисел (1-100) случайное число и предлагает пользователю его угадать.
# Пользователь вводит число. Если пользователь не угадал - предлагает пользователю угадать еще раз, пока он не угадает.
# Если угадал - спрашивает хочет ли он повторить игру (Y/N). Если Y - повторить. N - Прекратить
#
# Добавить счетчик попыток за которые он может угадать число.
# На каждом шаге угадывания числа сообщайте пользователю сколько попыток у него осталось.
# Если пользователь не смог угадать за отведенное количество попыток сообщить ему об окончании (Looser!).
#
# Добавить сообщения-подсказки. Если пользователь ввел число, и не угадал - сообщать:
# "Холодно" если разница между загаданным и введенным числами больше 10, "Тепло" - если от 5 до 10 и "Горячо"
# если от 4 до 1.
def welcome():
    print(f"Let's play a game?) The computer riddled the number from 0 to 100 your task is to guess the number in three\
tries. Good luck) ")


def new_game():
    while True:
        my_game = input("Let's play the game?( y - da, n - net): ")
        if my_game.isalpha():
            if my_game == 'Y' or my_game == 'y':
                print('Good luck')
                return True
            elif my_game == 'N' or my_game == 'n':
                return False
            else:
                print("Not 'y' and not 'n'")
        break


def numeric_counting():
    from random import randint
    x = randint(0, 100)
    errors = 0

    while errors < 10:
        number = input('Enter the number: ')
        if number.isdigit():
            number = int(number)
            if number < x:
                print("Cold!")
            elif number > x:
                print("Hot!")
            else:
                print(f"You guessed it - {x}!")
                break
            errors += 1
            print(f'You have {10 - errors} attempts left')
            if errors >= 10:
                print('LOSER(')
        else:
            print("Not a number")


def game():
    welcome()
    if new_game() is True:
        numeric_counting()
    else:
        print('Game over!')


game()
