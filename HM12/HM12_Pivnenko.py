# Напишите декоратор, который бы сообщал о времени выполнения функции
# Напишите переметризованный декоратор, который бы выводил время выполнения функции в милисекундах или секундах или
# минутах (как выводить - определяет параметр декоратора)
import time
from datetime import datetime


def my_decorator(func):
    def wrapper(*args, **kwargs):
        start_time = datetime.now()
        end_time = datetime.now()
        print('Duration program: {}'.format(end_time - start_time))
        res = func(*args, **kwargs)
        return res

    return wrapper


@my_decorator
def game1():
    print('hello')


game1()


def parametrized_decorator(arg_type=None):

    def _wrapper_outer(func):

        def _wrapper_inner(*f_args, **f_kwargs):
            seconds = time.time()
            if arg_type == 'seconds':
                # seconds = time.time()
                (time.time() - seconds)
                print(["{} seconds ".format(seconds)])
            elif arg_type == 'minutes':
                minutes = seconds % 3600 // 60
                (time.time() - minutes)
                print(["{} minutes".format(minutes)])
            elif arg_type == 'hour':
                hour = seconds//3600
                (time.time() - hour)
                print(["{} hour".format(hour)])
            else:
                raise TypeError

            res = func(*f_args, **f_kwargs)

            return res

        return _wrapper_inner

    return _wrapper_outer


@parametrized_decorator('seconds')
def game2():
    print('┈┈┈┈┈┈┈┈☆┈┈┈┈┈┈┈┈')


game2()

# def parametrized_decorator(arg_type):
#
#     def _wrapper_outer(func):
#
#         def _wrapper_inner(*f_args, **f_kwargs):
#             seconds = time.time()
#             (time.time() - seconds)
#             print(["{} seconds ".format(seconds)])
#             minutes = seconds % 3600 // 60
#             (time.time() - minutes)
#             print(["{} minutes".format(minutes)])
#             hour = seconds//3600
#             (time.time() - hour)
#             print(["{} hour".format(hour)])
#             res = func(*f_args, **f_kwargs)
#
#             return res
#
#         return _wrapper_inner
#
#     return _wrapper_outer
#
#
# @parametrized_decorator(str)
# def game():
#     print('┈┈┈┈┈┈┈┈☆┈┈┈┈┈┈┈┈')
#
#
# game()
