# Есть строка произвольного содержания. Написать код, который найдет в строке самое короткое слово,
# в котором присутствуют подряд две гласные буквы.
my_str = '''Britain is one of the most highly industrialised countries in the world: for every person employed in
agriculture 12 are employed in industry. The original base of British industry was coal-mining, iron and steel and
textiles. Today the most productive sectors include high-tech industries, chemicals, finance and the service sectors,
 especially banking, insurance and tourism.'''
# 6 букв могут обозначать гласные звуки: «A», «E», «I», «O», «U», «Y»;
for i in "aeiouy":
 my_str = my_str.replace(i, '!')

print(my_str)

start = -1
count = 0

while True:
    start = my_str.find('!!', start+1)
    if start == -1:
        break
    count += 1

print('The number of occurrences of the wildcard: ', count)

# Есть два числа - минимальная цена и максимальная цена. Дан словарь продавцов и цен на какой то товар у разных
# продавцов: { "citrus": 47.999, "istudio" 42.999, "moyo": 49.999, "royal-service": 37.245, "buy.ua": 38.324,
# "g-store": 37.166, "ipartner": 38.988, "sota": 37.720, "rozetka": 38.003}. Написать код, который найдет и выведет
# на экран список продавцов, чьи цены попадают в диапазон между нижней и верхней ценой. Например:
# lower_limit = 35.9
# upper_limit = 37.3
# > match: "g-store", "royal-service"
my_dict = {
    "citrus": 47.999,
    "istudio": 42.999,
    "moyo": 49.999,
    "royal-service": 37.245,
    "buy.ua": 38.324,
    "g-store": 37.166,
    "ipartner": 38.988,
    "sota": 37.720,
    "rozetka": 38.003
}

for key in my_dict:
 print(key, '->', my_dict[key])

# rez = sorted(my_dict.items(), key=lambda x: (x[1], x[0]))

print(max(my_dict, key=my_dict.get))

print(min(my_dict, key=my_dict.get))
