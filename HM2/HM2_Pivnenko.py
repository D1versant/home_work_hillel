# Попросить ввести свой возраст (можно исползовать константу или input()).
# - если пользователю меньше 7 - вывести “Где твои мама и папа?”
# - если пользователю меньше 18 - вывести “Мы не продаем сигареты несовершеннолетним!”
# - если пользователю больше 65 - вывести “Вы в зоне риска”
# - если у пользователя юбилей (10, 20, 40 и тд лет) - вывести “Где сертификат от вакцинации?”
# - в любом другом случае - вывести “Оденьте маску!"
age = None
try:
    age = int(input('Enter the age: '))
except:
    print('Exception! Not number')

print(f'Number is: {age}')

if age <= 7:
    print('Where are your mom and dad?')
elif age <= 18:
    print('We do not sell cigarettes to minors!')
elif age >= 65:
    print('You are at risk')
elif age == 10 or age == 20 or age == 40:
    print('Where is the certificate from the vaccination?')
else:
    print('Wear a mask!')
