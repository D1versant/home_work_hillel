# first=10, second=30. Вывести на экран результат математического взаимодействия (+, -, *, / и тд.) для этих чисел.
first = 10
second = 30
third = first + second
print(third)
fourth = first - second
print(fourth)
fifth = first * second
print(fifth)
sixth = first / second
print(sixth)
seventh = first ** second
print(seventh)
eighth = first // second
print(eighth)
ninth = first % second
print(ninth)