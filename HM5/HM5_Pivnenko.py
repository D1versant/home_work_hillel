# Написать функцию, принимающую два аргумента. Функция должна :
# - если оба аргумента относятся к числовым типам - вернуть их произведение,
# - если к строкам - соединить в одну строку и вернуть,
# - если первый строка, а второй нет - вернуть словарь (dict), в котором ключ - первый аргумент, значение - второй
# в любом другом случае вернуть кортеж (tuple) из аргументов

# def my_function (arg1, arg2):





# Пишем игру ) Программа выбирает из диапазона чисел (1-100) случайное число и предлагает пользователю его угадать.
# Пользователь вводит число. Если пользователь не угадал - предлагает пользователю угадать еще раз, пока он не угадает.
# Если угадал - спрашивает хочет ли он повторить игру (Y/N). Если Y - повторить игру. N - закончить
def welcome():
    print(f"Let's play a game?) The computer riddled the number from 0 to 100 your task is to guess the number in three\
tries. Good luck) ")


def numeric_counting():
    from random import randint
    x = randint(0, 100)
    errors = 0

    while errors < 3:
        number = input('Enter the number: ')
        if number.isdigit():
            number = int(number)
            if number < x:
                print("You didn't guess!")
            elif number > x:
                print("You didn't guess!")
            else:
                print(f"You guessed it - {x}!")
                break
            errors += 1
            print(f'You have {3 - errors} attempts left')
            if errors >= 3:
                print('You have exhausted three attempts(')
        else:
            print("Entered is not a number")

def game():
    welcome()
    numeric_counting()


game()

# Пользователь вводит строку произвольной длины. Написать функцию, которая должна вернуть словарь следующего содержания:
# ключ - количество букв в слове, значение - список слов с таким количеством букв.
# отдельным ключем, например "0", записать количество пробелов.
# отдельным ключем, например "punctuation", записать все уникальные знаки препинания, которые есть в тексте.
my_str = input(f'Enter sentence: ')

punctuation = []
my_letters = []

def spec_symbol():
    for symbol in my_str:
        if ".,;:!?*()_\/'".find(symbol) != -1:
            punctuation.append(symbol)
    x = tuple(punctuation)
    return x


def letters():
    for symbol in my_str:
        if "qwertyuiopasdfghjklzxcvbnm".find(symbol) != -1:
            my_letters.append(symbol)
    x = tuple(my_letters)
    return x


def space():
    my_str.count(' ')
    return my_str


def words():
     import re
     result = re.findall(r'[qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM]\w+', my_str)
     return result


def number_letters():
    import collections
    count_dict = dict(collections.Counter(my_str))
    return count_dict

my_dict = {
    'Number of special characters': spec_symbol(),
    'letters': letters(),
    'Number of space': space(),
    'Word count': words(),
    'Number of letters': number_letters(),
    }


for i in my_dict.items():
    print(i)
