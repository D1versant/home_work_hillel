class Kat:
    number_paws = ''
    sound_produced = ''

    def __init__(self, new_number=None, new_sound=None):
        if new_number is not None:
            self.number_paws = new_number
        if new_sound is not None:
            self.sound_produced = new_sound

    def animal(self):
        return f'I have {self.number_paws} paws and make a sound {self.sound_produced}'


kat1 = Kat(4, '<meow>')

print(kat1.animal())

kat1.nickname = "pet name 'Dori'"

print(kat1.nickname)
