# Напишите класс, отвечающий за животное. Реализуйте в классе атрибуты :
# количество лап, издаваемый звук, кличка. Реализуйте в классе метод "издать звук".
# Количество лап и звук задается при инициализации и имеет ограничения (ограничения придумайте сами).
# Кличка дается после инициализции. Создайте несколько обьектов класса, напр кошка, собака, птица, и тд.
class Kat:
    number_paws = ''
    sound_produced = ''
    nickname = ''

    def __init__(self, new_number=None, new_sound=None):
        if new_number is not None:
            self.number_paws = new_number
        if new_sound is not None:
            self.sound_produced = new_sound
        self.nickname = 'Dori'

    def animal(self):
        return f"I have {self.number_paws} paws and make a sound {self.sound_produced}, pet name '{self.nickname}'"


kat1 = Kat(4, "'meow' (^˵◕ω◕˵^)")

print(kat1.animal())


class Dog:
    number_paws = ''
    sound_produced = ''
    nickname = ''

    def __init__(self, new_number=None, new_sound=None):
        if new_number is not None:
            self.number_paws = new_number
        if new_sound is not None:
            self.sound_produced = new_sound
        self.nickname = 'Boy'

    def animal(self):
        return f"I have {self.number_paws} paws and make a sound {self.sound_produced}, pet name '{self.nickname}'"


dog1 = Dog(4, "'gav' (▽◕ ᴥ ◕▽)")

print(dog1.animal())


class Bear:
    number_paws = ''
    sound_produced = ''
    nickname = ''

    def __init__(self, new_number, new_sound):
        if new_number == 4:
            self.number_paws = new_number
        if new_sound[1] == 'u':
            self.sound_produced = new_sound
        self.nickname = 'Brown'

    def animal(self):
        return f"I have {self.number_paws} paws and make a sound {self.sound_produced}, pet name '{self.nickname}'"


bear1 = Bear(4, "'uv-uv' ʕ ᵔᴥᵔ ʔ")

print(bear1.animal())
