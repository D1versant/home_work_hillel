# В отдельном файле (пусть будет lib.py) написать функцию, которая требует от пользователя ответить да или нет (Y/N) и
# возвращает True/False в зависимости от того, что он ввел. В основном файле (пусть будет main_file.py) попросить
# пользователя ввести с клавиатуры строку и вывести ее на экран. Используя импортированную из lib.py функцию спросить
# у пользователя, хочет ли он повторить операцию (Y/N). Повторять пока пользователь отвечает Y и прекратить когда
# пользователь скажет N.
import lib

my_str = input('Enter the line: ')
print(my_str)
lib.new_game()

# Модифицируем ДЗ2. Напишите с помощью функций!. Помните о Single Respinsibility! Попросить ввести свой
# возраст (можно использовать константу или input()). Пользователь ввел значение возраста [year number] а на место
# [year string] нужно поставить правильный падеж существительного "год", который зависит от значения [year number].
# -если пользователь ввел непонятные данные (ничего не ввел, ввел не число, неактуальный возраст и тд.) - вывести
# “не понимаю”
# -если пользователю меньше 7 - вывести “Тебе [year number] [year string], где твои мама и папа?”
# -если пользователю меньше 18 - вывести “Тебе [year number] [year string], а мы не продаем сигареты несовершеннолетним”
# -если пользователю больше 65 - вывести “Вам уже [year number] [year string], вы в зоне риска”!
# -в любом другом случае - вывести “Оденьте маску, вам же [year number] [year string]!”
#  Например:
#  Тебе 1 год, где твои мама и папа?
#  Оденьте маску, вам же 23 года!
#  Вам уже 68 лет, вы в зоне риска!
age = input('Enter the age: ')
if age.isdigit():
    age = int(age)
else:
    print("Don't understand")

dmy = {'y': age}
words = {'y': ['лет', 'год', 'года']}
out = []

for k, v in dmy.items():
    letters = v % 10
    if v == 0 or letters == 0 or letters >= 5 or v in range(11, 19):
        st = str(v), words[k][0]
    elif letters == 1:
        st = str(v), words[k][1]
    else:
        st = str(v), words[k][2]
    out.append(" ".join(st))


def my_age():
    if age <= 7:
        print(f'You {out} where are your mom and dad?')
    elif age <= 18:
        print(f"You {out} and we don't sell cigarettes to minors!")
    elif age >= 65:
        print(f"You're already {out} and you're in the  risk zone")
    else:
        print(f'Wear a mask! your are  {out}')


my_age()